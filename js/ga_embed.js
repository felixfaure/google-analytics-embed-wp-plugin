gapi.analytics.ready(function() {
	//https://ga-dev-tools.appspot.com/embed-api/
	//https://ga-dev-tools.appspot.com/embed-api/server-side-authorization/
	//https://developers.google.com/identity/protocols/OAuth2ServiceAccount#creatinganaccount
	//http://stackoverflow.com/questions/28751995/how-to-obtain-google-service-account-access-token-javascript
	//https://developers.google.com/analytics/devguides/reporting/embed/v1/component-reference

	//Aller sur la console google : https://console.developers.google.com/project
	//Créer un projet
	//Aller dans "APIS" et autoriser Google Analytics
	//Aller dans "API et authentification" => "Identifiants" => "Ajouter des identifiants" => "Compte de service", choisir json et créer.
	//On télécharge le fichier json, on peut l'ouvrir et voir les infos
	//Aller dans la vue analytics dont on veut voir les stats et rentrer le mail du compte service
	//Modifier les 3 variables suivantes :

	//vue dont on verra les stats
	var view = "102813720";
	//Mail compte service (dans le json)
	var mail_service_count = "650557469905-knq7m8ikeh3eosr8dk0chubtna4r133d@developer.gserviceaccount.com";
	//Clé privée (dans le json)
	var private_key = "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDN2TRA5piWfNqb\nfj81gRDTqI2BIv+sVwpL4j85EZarYjVbl8KXI9OvQCLIa9bzm/TWaUp/OCvoFRNV\nP+X4yZug63b/DEmJiDdLyzEOCYkGQEYTbwAcUm0X8aUUUjFqJlWGgW1qaDAvQMO6\nRHhleINzGJ841ntXOZCybsgww2bdWZhpPi8DN1dCrJgCajmRff+ThdhdhnOrVQLT\na2n9AtQthuvtYI7+BUD21lCuBRaPs8qAcqxHBWbMzJNLtAhG+1Qu5+8IOKfDB1/K\nhVxnfNCQ9fMcJYZzZbIHkBUv6beWWSsCQWntVPAcr8vkDrevbZqRo6M4XVgB3R+H\n42osneG3AgMBAAECggEAdd05WzPuCLTCoyfdX3BgRyEKrPdyN6a64YpRMwcMI2Of\nM6uoosvDyGPLxsBgmqogvagaHM/36/ItWLG2pwYhEznfr9fEP3h8WOymUOhKygaB\nWV1ETfMwPfaB1Cnf22zJqbNwB+5SMKw/pebQnSEs+vkNKgk9JEt0u4ZNIAayXmiI\nDRsjfxer77fclSpkGezKeKm186afgxvI+whj90AtyHTSSGxoECQTBlS7ihZN0yU9\nPszilMhkJP+miJ3cqdgu1Ez1XYzXREqnMxwwi6BfKKBEcW2ts4tJpecuVX9e5oJP\nwaVeutJnao+eYnnlZPkeJzfEvMRdJCh1ErsITnWj8QKBgQD3QjeLl09CTmhrHQcQ\nfvy7M5Bjr3l3ndXyld7NlRDm8Tumm/vPu8GJ0CRMIpFgj0VPeX9UZpcRrg5mJU3r\nviheN/4iPx7P8j9c1fapR+3GiZ9zsZY7DaQwPwKGioMUPAwH5f/U4rdQoZFvOwVs\nLkDXpUHLxSmb7zOKnm1BTI6eaQKBgQDVIDWXRQ2vZ35G/Y7sKYmDJ9v6UN7MN8Vf\nIDYsuvudJzWlfNiCIQF4tl0xQhTIIb5YLfOkDgJ+6qMOCFFEmk9pgCfA/lNhzH9f\ngznJggXgCB6YyHAmkfaaKhOJAaNTL9UJ7GQucTutGt12qKOCgyEZ/6Q92KqxTBZB\np06HnFK7HwKBgQD0mztcCypLzEmcfbD5bscTYyWamIOwBCMdw4oRE0EPVuLOJk3s\n6i/C6HigISp0vgexYd7HXl5hDTINdyYo64v/+aCdvJf83Ag3DSzhttjRqmBmEe8v\nKIWffRZAh1K5d9XnEnAO6OK1D8FSqq7ooLFhJIl8zuYk73Sr06L+EICe0QKBgCK8\nCh/qQC27J0h4oKd6qY+3mlF0hqsv17oGLO4jV0iokES1US/8IxpCVnPlui9X2IbU\njmXJZry6jvKFhoEwyQp/uXrpuDszMfsmDu/5ge2eRSZPXH/cwB0mCGMG4lB3djgU\nz8sF51zyCPAC0gs+RdHP6TuvKTqWFPaERvcxEgavAoGBAI//OXqo4zdamsZygzI3\nLzAdzfZtz7E8ggz2pOmJX5TVKcu7Yk+VlwLSAqbjONkYaA3q7cX6XaXT8k3EPxZZ\nQwJIxesSbZyL0nfXGy+Yd5KVJq0jqKuMDCfOdBG0uHMeLMf8iukMhzo8L0VpSa3D\nXoe3hBqYqcU4dtUskFS4aHxZ\n-----END PRIVATE KEY-----\n";

	var pHeader = {"alg":"RS256","typ":"JWT"}
	var sHeader = JSON.stringify(pHeader);

	var pClaim = {};
	pClaim.aud = "https://www.googleapis.com/oauth2/v3/token";
	pClaim.scope = "https://www.googleapis.com/auth/analytics.readonly";
	pClaim.iss = mail_service_count;
	pClaim.exp = KJUR.jws.IntDate.get("now + 1hour");
	pClaim.iat = KJUR.jws.IntDate.get("now");
	var sClaim = JSON.stringify(pClaim);

	var sJWS = KJUR.jws.JWS.sign(null, sHeader, sClaim, private_key);

	var XHR = new XMLHttpRequest();
	var urlEncodedData = "";
	var urlEncodedDataPairs = [];

	urlEncodedDataPairs.push(encodeURIComponent("grant_type") + '=' + encodeURIComponent("urn:ietf:params:oauth:grant-type:jwt-bearer"));
	urlEncodedDataPairs.push(encodeURIComponent("assertion") + '=' + encodeURIComponent(sJWS));
	urlEncodedData = urlEncodedDataPairs.join('&').replace(/%20/g, '+');

	// We define what will happen if the data are successfully sent
	XHR.addEventListener('load', function(event) {
	    var response = JSON.parse(XHR.responseText);
	    // console.log("Succès !");
	    // console.log(response);
	    token = response["access_token"];
	    launch_stats(token,view);
	});

	// We define what will happen in case of error
	XHR.addEventListener('error', function(event) {
	    console.log('Oops! Something went wrong bitch.');
	});

	XHR.open('POST', 'https://www.googleapis.com/oauth2/v3/token');
	XHR.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	XHR.send(urlEncodedData);
});

//Debounce (from underscore)
debounce = function(func, wait, immediate) {
	var result;
	var timeout = null;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) result = func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) result = func.apply(context, args);
		return result;
	};
};

function launch_stats(token, view) {
	//Autorisation
	gapi.analytics.auth.authorize({
		'serverAuth': {
			'access_token': token
		}
	});

	//Garde un tableau de tous les graph pour les redimensionner quand la taille fenetre change
	var originalDataChart = gapi.analytics.googleCharts.DataChart;
	var dataCharts = [];
	gapi.analytics.googleCharts.DataChart = function DataChart(options) {
		var dataChart = new originalDataChart(options);
		dataCharts.push(dataChart);
		return dataChart;
	};

	// //New active user
	// var activeUsers = new gapi.analytics.ext.ActiveUsers({
	// 	container: 'activeUsers',
	// 	pollingInterval: 5
	// });
	// activeUsers.set({
	// 	'ids': 'ga:'+view
	// })
	// .execute();
	// //Animation pour voir quand un nouveau visiteur arrive ou part
	// activeUsers.once('success', function() {
	// 	var element = this.container.firstChild;
	// 	var timeout;

	// 	this.on('change', function(data) {
	// 		var element = this.container.firstChild;
	// 		var animationClass = data.delta > 0 ? 'is-increasing' : 'is-decreasing';
	// 		element.className += (' ' + animationClass);

	// 		clearTimeout(timeout);
	// 		timeout = setTimeout(function() {
	// 			element.className =
	// 			element.className.replace(/ is-(increasing|decreasing)/g, '');
	// 		}, 3000);
	// 	});
	// });

	//Création du composant dateRange
	var dateRangeSelector = new gapi.analytics.ext.DateRangeSelector({
		container: 'dateRangeVisits'
	})
	.set({
		'start-date': '30daysAgo',
		'end-date': 'yesterday'
	})
	.execute();

	//Créer un diagramme
	var chartVisits = new gapi.analytics.googleCharts.DataChart({
		query: {
			'ids': 'ga:'+view,
			'start-date': '30daysAgo',
			'end-date': 'yesterday',
			'metrics': 'ga:sessions',
			'dimensions': 'ga:date'
		},
		chart: {
			'container': 'chartVisits',
			'type': 'LINE',
			'options': {
				'width': '100%',
				'title': 'Nombre de visites',
				// 'subtitle': 'in millions of dollars (USD)'
				// 'colors': ['#e0440e', '#e6693e', '#ec8f6e', '#f3b49f', '#f6c7b6'],
				'legend': false,
			}
		}
	});
	chartVisits.execute();

	var chartCountries = new gapi.analytics.googleCharts.DataChart({
		query: {
			'ids': 'ga:'+view,
			'start-date': '30daysAgo',
			'end-date': 'yesterday',
			'metrics': 'ga:sessions',
			'dimensions': 'ga:country'
		},
		chart: {
			'container': 'chartCountries',
			'type': 'GEO',
			'options': {
				'width': '100%',
			}
		}
	});
	chartCountries.execute();

	var chartLang = new gapi.analytics.googleCharts.DataChart({
		query: {
			'ids': 'ga:'+view,
			'start-date': '30daysAgo',
			'end-date': 'yesterday',
			'metrics': 'ga:sessions',
			'dimensions': 'ga:language',
			'sort': '-ga:sessions',
			'filters': 'ga:language!=(not set)',
			'max-results': '8'
		},
		chart: {
			'container': 'chartLang',
			'type': 'TABLE',
			'options': {
				'width': '100%',
			}
		}
	});
	chartLang.execute();

	var chartBrowser = new gapi.analytics.googleCharts.DataChart({
		query: {
			'ids': 'ga:'+view,
			'start-date': '30daysAgo',
			'end-date': 'yesterday',
			'metrics': 'ga:sessions',
			'dimensions': 'ga:browser',
			'sort': '-ga:sessions',
			// 'filters': 'ga:pagePathLevel1!=/',
			'max-results': 6
		},
		chart: {
			'container': 'chartBrowser',
			'type': 'PIE',
			'options': {
				'title': 'Navigateurs utilisés',
				'width': '100%',
				'pieHole': 4/9,
			}
		}
	});
	chartBrowser.execute();

	var chartMobiles = new gapi.analytics.googleCharts.DataChart({
		query: {
			'ids': 'ga:'+view,
			'start-date': '30daysAgo',
			'end-date': 'yesterday',
			'metrics': 'ga:sessions',
			'dimensions': 'ga:deviceCategory',
			'sort': '-ga:sessions',
			// 'filters': 'ga:pagePathLevel1!=/',
			'max-results': 3
		},
		chart: {
			'container': 'chartMobiles',
			'type': 'PIE',
			'options': {
				'title': 'Types d\'appareils utilisés',
				'width': '100%',
				'pieHole': 4/9,
			}
		}
	});
	chartMobiles.execute();

	var chartPages = new gapi.analytics.googleCharts.DataChart({
		query: {
			'ids': 'ga:'+view,
			'start-date': '30daysAgo',
			'end-date': 'yesterday',
			'metrics': 'ga:pageviews',
			'dimensions': 'ga:pagePath',
			'sort': '-ga:pageviews',
			// 'filters': 'ga:pagePathLevel1!=/',
			'max-results': 7
		},
		chart: {
			'container': 'chartPages',
			'type': 'TABLE',
			'options': {
				'width': '100%',
			}
		}
	});
	chartPages.execute();

	//Evenement quand les input dateRange changent
	dateRangeSelector.on('change', function(data) {
		chartVisits.set({query: data}).execute();
		chartCountries.set({query: data}).execute();
		chartLang.set({query: data}).execute();
		chartBrowser.set({query: data}).execute();
		chartMobiles.set({query: data}).execute();
		chartPages.set({query: data}).execute();
	});

	jQuery( '.DateRangeSelector_item input[type="date"]' ).each(function(index, el) {
		var $altFiel = jQuery(this).next('input[type="hidden"]');
		jQuery(this).datepicker({
			dateFormat		:	'dd/mm/yy',
			altField		:	$altFiel,
			altFormat		:	'yy-mm-dd',
			changeYear		:	true,
			yearRange		:	"-5:+0",
			changeMonth		:	true,
			showButtonPanel	:	false,
			closeText       :   "Valider",
		    currentText     :   "Aujourd'hui",
		    monthNames      :   strings.months,
		    monthNamesShort :   strings.months_short,
		    dayNames        :   strings.days,
		    dayNamesShort   :   strings.days_short,
		    dayNamesMin     :   strings.days_min,
		    monthStatus     :   strings.month_status,
		    isRTL           :   strings.isRTL,
		    firstDay        :   1,
		    hideIfNoPrevNext:   true,
		    maxDate         :   0,
		    minDate         :   "-5y", //"-5d -1m -1y"
		    // minDate         :   new Date(2007, 1 - 1, 1),
		}).attr({
			type: 'text',
			placeholder: 'jj/mm/aaaa'
		});
	});

	//On redimensionne les graph quand la taille d ela fenetre change
	window.addEventListener('resize', debounce(function() {
		dataCharts.forEach(function(dataChart) {
			dataChart && dataChart.execute();
		});
	}, 200), false);
}
