gapi.analytics.ready(function() {

  var nDaysAgo = /(\d+)daysAgo/;
  var dateFormat = /\d{4}\-\d{2}\-\d{2}/;


  function convertDate(str,fr) {
    fr = typeof fr !== 'undefined' ? fr : false;

    // If str is in the proper format, do nothing.
    if (dateFormat.test(str)) return str;

    var match = nDaysAgo.exec(str);
    if (match) {
      return daysAgo(+match[1],fr);
    } else if (str == 'today') {
      return daysAgo(0,fr);
    } else if (str == 'yesterday') {
      return daysAgo(1,fr);
    } else {
      throw new Error('Cannot convert date ' + str);
    }
  }


  function daysAgo(numDays,fr) {
    fr = typeof fr !== 'undefined' ? fr : false;
    var date = new Date();
    date.setDate(date.getDate() - numDays);
    var month = String(date.getMonth() + 1);
    month = month.length == 1 ? '0' + month: month;
    var day = String(date.getDate());
    day = day.length == 1 ? '0' + day: day;
    if(!fr) return date.getFullYear() + '-' + month + '-' + day;
    else return day + '/' + month + '/' + date.getFullYear();
  }

  gapi.analytics.createComponent('DateRangeSelector', {
    execute: function() {
      var options = this.get();
      options['start-date'] = options['start-date'] || '7daysAgo';
      options['end-date'] = options['end-date'] || 'yesterday';

      // Allow container to be a string ID or an HTMLElement.
      this.container = typeof options.container == 'string' ?
        document.getElementById(options.container) : options.container;

      // Allow the template to be overridden.
      if (options.template) this.template = options.template;

      this.container.innerHTML = this.template;
      var dateInputs = this.container.querySelectorAll('input');

      // var date= '21/01/2015';
      // var d=new Date(date.split("/").reverse().join("-"));

      this.startDateInputShow = dateInputs[0];
      this.startDateInput = dateInputs[1];
      this.startDateInputShow.value = convertDate(options['start-date'],true);
      this.startDateInput.value = convertDate(options['start-date']);
      this.endDateInputShow = dateInputs[2];
      this.endDateInput = dateInputs[3];
      this.endDateInputShow.value = convertDate(options['end-date'],true);
      this.endDateInput.value = convertDate(options['end-date']);

      // this.setValues();
      // this.setMinMax();

      this.container.onchange = this.onChange.bind(this);
      return this;
    },

    onChange: function() {
      this.setValues();
      this.setMinMax();
      this.emit('change', {
        'start-date': this['start-date'],
        'end-date': this['end-date']
      });
    },

    setValues: function() {
      this['start-date'] = this.startDateInput.value;
      this['end-date'] = this.endDateInput.value;
    },

    setMinMax: function() {
      this.startDateInput.max = this.endDateInput.value;
      this.endDateInput.min = this.startDateInput.value;
    },

    template:
      '<div class="DateRangeSelector">' +
      '  <div class="DateRangeSelector_item">' +
      '    <label>Début</label> ' +
      '    <input type="date" placeholder="yyyy-mm-dd">' +
      '    <input type="hidden">' +
      '  </div>' +
      '  <div class="DateRangeSelector_item">' +
      '    <label>Fin</label> ' +
      '    <input type="date" placeholder="yyyy-mm-dd">' +
      '    <input type="hidden">' +
      '  </div>' +
      '</div>'
  });

});
