<?php
/*
Plugin Name: Google Analytics Embed plugin
Plugin URI: #
Version: 1.0
Author: David Félix-Faure
Author URI: http://www.felixfaure.fr/
Description: A simple WordPress plugin for Google Analytics embed.
License: GPL2
Text Domain: gaedff
*/


class GAEdff {
	private $redirect_for_admin = true;

	public function __construct() {
		add_action('admin_menu', array($this, 'add_GAEdff_page'));
		add_action('admin_enqueue_scripts',array($this, 'add_GAEdff_scripts'),10,1);

		//Redirection après login
		add_filter("login_redirect", array($this, 'redirect_after_login'), 10, 3);
		//Redirection pour éviter qu'un utilisateur pas admin arrive sur le dashboard
		add_action('admin_init', array($this, 'prevent_dispay_dashboard'));
	}

	public function add_GAEdff_page() {
		//Suppression du dashboard
		remove_menu_page('index.php');
		//Ajout de la page
		add_menu_page(__('Statistiques','gaedff'),__('Statistiques','gaedff'),'read','statistiques',array($this, 'GAEdff_page'),'dashicons-chart-bar',-1);
	}

	public function GAEdff_page(){
		include(__DIR__.'/includes/statistiques.php');
	}

	public function add_GAEdff_scripts($page) {
		if( $page == "toplevel_page_statistiques" ) {
			wp_enqueue_style( 'gaedff_css', plugins_url( 'css/gaedff.css', __FILE__ ), array(), NULL );
			wp_enqueue_style( 'jquery-ui-datepicker-default', 'http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css', array(), NULL );
			wp_enqueue_script( 'jquery' );
			wp_enqueue_script( 'jquery-ui-datepicker' );
			wp_enqueue_script( 'gaedff_jws', plugins_url( 'js/jws.js', __FILE__ ), array(), NULL, true );
			wp_enqueue_script( 'gaedff_date_range', plugins_url( 'js/date-range-selector.js', __FILE__ ), array(), NULL, true );
			// wp_enqueue_script( 'gaedff_active_users', plugins_url( 'js/active-users.js', __FILE__ ), array(), NULL, true );
			wp_enqueue_script( 'gaedff_main', plugins_url( 'js/ga_embed.js', __FILE__ ), array('gaedff_jws','gaedff_date_range','jquery','jquery-ui-datepicker'), NULL, true );
			global $wp_locale;
			wp_localize_script( 'gaedff_main', 'strings', array( 'months' => array_values($wp_locale->month), 'months_short' => array_values($wp_locale->month_abbrev), 'days' => array_values($wp_locale->weekday), 'days_short' => array_values($wp_locale->weekday_abbrev), 'days_min' => array_values($wp_locale->weekday_initial), 'month_status' => __( 'Montrer un mois différent', 'gaedff' ), 'isRTL' => (isset($wp_locale->is_rtl) ? $wp_locale->is_rtl : false) ) );
		}
	}

	public function redirect_after_login($redirect_to, $request, $user) {
		if($this->redirect_for_admin) {
			return admin_url('admin.php?page=statistiques');
		} else {
			global $user;
			if ( isset( $user->roles ) && is_array( $user->roles ) && !in_array( 'administrator', $user->roles ) ) return admin_url('admin.php?page=statistiques');
			else return $redirect_to;
		}
	}

	public function prevent_dispay_dashboard() {
    global $pagenow;
    if ( $this->redirect_for_admin && 'index.php' == $pagenow && empty($_GET['page']) ) {
			wp_redirect(admin_url('admin.php?page=statistiques'));
		} else {
			$user = wp_get_current_user();
			if ( empty($user) ) return false;
			if ( !in_array( "administrator", (array) $user->roles) && 'index.php' == $pagenow && empty($_GET['page']) ) wp_redirect(admin_url('edit.php?post_type=page'));
		}
	}

}

add_action( 'init', 'GAEdff' );
function GAEdff() {
	$GAEdff = new GAEdff();
}
?>
