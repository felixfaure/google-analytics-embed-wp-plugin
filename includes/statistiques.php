<div class="wrap">
    <h1><?php _e('Statistiques','gaedff');?></h1>

    <div id="gaedff">
        <div class="dashboard">
            <div id="dateRangeVisits"></div>
            <!-- <div id="activeUsers"></div> -->
        </div>
        <div class="dashboard">
            <div id="chartVisits" style="height:300px;"></div>
        </div>
        <div class="clearfix">
            <div class="dashboard dashboard-half">
                <div id="chartCountries" style="height:300px;"></div>
            </div>
            <div class="dashboard dashboard-half">
                <div id="chartLang" style="height:300px;"></div>
            </div>
        </div>
        <div class="clearfix">
            <div class="dashboard dashboard-half">
                <div id="chartBrowser" style="height:300px;"></div>
            </div>
            <div class="dashboard dashboard-half">
                <div id="chartMobiles" style="height:300px;"></div>
            </div>
        </div>
        <div class="clearfix">
            <div class="dashboard dashboard-half">
                <div id="chartPages" style="height:300px;"></div>
            </div>
        </div>
    </div>



    <script>
    //API GA
    (function(w,d,s,g,js,fs){
      g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
      js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
      js.src='https://apis.google.com/js/platform.js';
      fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
    }(window,document,'script'));
    </script>
</div>
